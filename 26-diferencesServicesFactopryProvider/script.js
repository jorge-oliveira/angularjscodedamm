var application = angular.module('mainApp', []);

application.service('fromService', function () {

    this.message = "this is from Service!";
});

application.factory('fromFactory', function(){

    var factory = {};
    factory.message = "This it's from factory!";
    return factory;
});

application.provider('fromProvider', function(){

    var m1= "this it's from provider!";
    return{
        setAName : function(name){
            m1 += " " + name;
        },
        $get: function(){
            return {
                message: m1
            }
        }
    }

});

application.config(function (fromProviderProvider) {

    fromProviderProvider.setAName("Jorge");
});


application.controller('app', function ($scope, fromService, fromFactory, fromProvider) {
    $scope.greetMessage = [fromFactory, fromService, fromProvider];

})