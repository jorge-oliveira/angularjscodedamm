var application = angular.module('mainApp', []);

// um service é um singleton
application.service('random', function () {

    // this neste caso é este random service
    var num = Math.floor(Math.random() * 10);
    this.generate = function () {
        return num;
    };
});

application.controller('app', function ($scope, random) {
    $scope.generateRandom = function () {
        $scope.randomNumber = random.generate();
    }
});

// -----------> Run the service
// -----------> PASSED the reference of the service